

// SECTION - Exponent operator
 let firstNum = Math.pow(8,2);
 console.log(firstNum);

 // es6
 let secondNum = 8 ** 2;
 console.log(secondNum);

 // Section - template Literals
 /*
 	allows to write strings without concantination operator. (+)
 	it also helps greatly in terms of code readability
 */

 let name = "John";

 //pre-es6
 let message = "Hello "+name+ "! Welcome to programming!";
 // Message without template literals: message
 console.log('Message without template literals: '+message);

 //es6 update
 message = `Hello ${name}! Welcome to programming!`;
 console.log(`Message without template literals: ${message}`);

 // multiple line
 const anotherMessage = `
 ${name} won the math competition.
 He won it by solving the problem 8 ** 2 with the solution of ${firstNum}.
 `;
console.log(anotherMessage);

// mini-activity

let interestRate = .15;
let principal = 1000;
let calc = principal*interestRate;

console.log(`The total interest rate ${interestRate} of the principal ${principal} is: ${calc}`);

console.log(`The total interest on your savings account is: ${interestRate*principal}`);

// Section - array destructuring
/*
	- allow to unpack elements in array to distinct variables
	- name array elements with variables instead of using index numbers
*/

const fullName = ['Juan', "Dela", "Cruz"];
console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);

// array destructuring

const [firstName, middleName, lastName] = fullName;
console.log(firstName);
console.log(middleName);
console.log(lastName);

// using template literals
console.log(`Hello ${firstName} ${middleName} ${lastName}!`);

//mini activity

// pre-object destructuring
let person = {
	givenName: "Jane",
	maidenName: "Dela",
	familyName: "Cruz",
}
console.log(person.firstName);
console.log(person.maidenName);
console.log(person.familyName);
console.log(`Hello ${person.givenName} ${person.maidenName} ${person.familyName}`);

// object destructuring
// allow us to unpack properties of object into distinct variables
// syntax: ;et/const { propertyA, propertyB, propertyC} = objectName;
// note: will return an error if wrong usage of property name
const { givenName, maidenName, familyName} = person;
console.log(givenName);
console.log(maidenName);
console.log(familyName);
console.log(`Hello ${person.givenName} ${person.maidenName} ${person.familyName}`);

//using object destructuring as a paramter of a function

function getFullName ({givenName,maidenName,familyName}){
	console.log(`${givenName} ${maidenName} ${familyName}`);
};

getFullName(person);

//mini activity

let pet = {
	dogname: "Nojie boy",
	trick: "Vertical tumbling",
	treat: "Beef pares"
};

function getDog({dogname,trick,treat}){
	console.log(`${dogname}, ${trick}`);
	console.log(`Good ${trick}!`);
	console.log(`Here is ${treat} for you!`);
}
getDog(pet);

// SECTION arrow function

// const hello = ()=>{
// 	console.log("Hello world!");
// };


//pre-es6 arrow function

// function printFullName(firstName, middleName, lastName){
// 	console.log(`${firstName} ${middleName} ${lastName}`);
// }
// printFullName("Portgas", "D", "Ace");

const printFullName = (firstName,middleName,lastName) => {
	console.log(`${firstName} ${middleName} ${lastName}`);
}

printFullName("Portgas", "D", "Ace");

//arrow function w/ loops
//pre-es6 arrow function
const students = ["John", "Jane", "Judy"];
students.forEach(function(student){
	console.log(`${student} is a student`);
});

//mini activity
// pre-arrow function with return
// const add = (x,y)=>{
// 	return x+y;
// };
// console.log(add(55,99));

// es-6 arrow function

const add = (x,y) => x+y;
let addVariable = add(5123,1233);
console.log(addVariable);

const greet = (name="User") =>{
	return `Good morning ${name}`
};
console.log(greet("John wick"));

// Section - class-bassed object Blueprints

class Car{
	constructor(brand,name,year){
		this.brand = brand;
		this.name = name;
		this.year = year;
	}
};

//frst instance
// using initializer and/or dot notation, create a car object using the Car class

const myCar = new Car("Ferarri","F40",1999);
console.log(myCar);
